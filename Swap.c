#include <stdio.h>
int main()
{
    int a,b;
    printf("Enter the two values to swap\n");
    scanf("%d %d",&a,&b);
    printf("Before swapping a=%d and b=%d\n",a,b);
    a=a+b;
    b=a-b;
    a=a-b;
    printf("Before swapping a=%d and b=%d\n",a,b);
    return 0;
}
